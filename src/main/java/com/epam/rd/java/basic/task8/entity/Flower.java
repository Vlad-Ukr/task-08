package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class Flower {
    String name;
    String soil;
    String origin;
    VisualParameters visualParameters;
    GrowingTips growingTips;
    String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(name, flower.name) && Objects.equals(soil, flower.soil) && Objects.equals(origin, flower.origin) && Objects.equals(visualParameters, flower.visualParameters) && Objects.equals(growingTips, flower.growingTips) && Objects.equals(multiplying, flower.multiplying);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, soil, origin, visualParameters, growingTips, multiplying);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +System.lineSeparator()+
                ", visualParameters=" + visualParameters +System.lineSeparator()+
                ", growingTips=" + growingTips +System.lineSeparator()+
                ", multiplying='" + multiplying + '\'' +System.lineSeparator()+
                '}';
    }
}
