package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.comparators.Comparators;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.FlowersList;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		FlowersList flowers= domController.getFlowersList();

		Comparators.SortFlowersByName(flowers);

		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
		System.out.println("dom issssss");
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		flowers = saxController.getFlowers();

		Comparators.SortFlowersByOrigin(flowers);

		outputXmlFile = "output.sax.xml";
		System.out.println("sax issssss");
		DOMController.saveToXML(flowers, outputXmlFile);
		STAXController staxController = new STAXController(xmlFileName);
		STAXController stax = new STAXController(xmlFileName);
		stax.parse();
		flowers = stax.getFlowers();

		Comparators.SortFlowersBySoil(flowers);

		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(flowers, outputXmlFile);
		System.out.println("stax issssss");
	}

}
