package com.epam.rd.java.basic.task8.comparators;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.FlowersList;

import java.util.Comparator;

public class Comparators {
    public static final Comparator<Flower> SORT_FLOWERS_BY_NAME=((o1, o2) -> o1.getName().compareTo(o2.getName()));
    public static final Comparator<Flower> SORT_FLOWERS_BY_SOIL=((o1, o2) -> o1.getSoil().compareTo(o2.getSoil()));
    public static final Comparator<Flower> SORT_FLOWERS_BY_ORIGIN=((o1, o2) -> o1.getOrigin().compareTo(o2.getOrigin()));
    public static void SortFlowersByName(FlowersList flowers){flowers.sort(SORT_FLOWERS_BY_NAME);}
    public static void SortFlowersBySoil(FlowersList flowers){flowers.sort(SORT_FLOWERS_BY_SOIL);}
    public static void SortFlowersByOrigin(FlowersList flowers){flowers.sort(SORT_FLOWERS_BY_ORIGIN);}
}
